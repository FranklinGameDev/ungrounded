using System;
using UnityEngine;


namespace UnityStandardAssets.Utility
{
    public class FollowTarget : MonoBehaviour
    {
        public Transform target;
        public Vector3 offset = new Vector3(0f, 7.5f, 0f);
        public bool useMousePosition;
        public bool useTransform;
        public bool useRigidBody;
        public bool useRigidBody2D;

        public ForceMode2D forceMode2D = ForceMode2D.Force;
        public ForceMode forceMode = ForceMode.Force;
        Rigidbody ourRigidBody;
        Rigidbody2D ourRigidBody2D;

        void Start ()
        {
            if (useRigidBody2D)
            {
                ourRigidBody2D = GetComponent<Rigidbody2D>();
            }
            if (useRigidBody)
            {
                ourRigidBody = GetComponent<Rigidbody>();
            }
        }

        private void LateUpdate()
        {
            if (useRigidBody2D)
            {
                ourRigidBody2D.AddForce(Vector3.Normalize(target.position - transform.position), forceMode2D );
            }
            if (useRigidBody)
            {
                ourRigidBody.AddForce(Vector3.Normalize(target.position - transform.position), forceMode );
            }
            if (useTransform)
            {
                transform.position = target.position + offset;
            }
            if ( useMousePosition )
            {
                Vector3 target = Camera.main.ScreenToWorldPoint(Input.mousePosition);
                target.z = transform.position.z;
                transform.position = Vector3.MoveTowards(transform.position, target, Time.deltaTime * 10f);
            }
        }

        public void Release()
        {
            useTransform = false;
            useRigidBody = false;
            useRigidBody2D = false;
            useMousePosition = false;
        }

        public void ActivateMouseFollow(bool activate)
        {
            useMousePosition = activate;
        }
    }
}
