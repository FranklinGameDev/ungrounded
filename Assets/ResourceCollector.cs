﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ResourceCollector : MonoBehaviour {

    public List<GameObject> targetResources = new List<GameObject>();
    
    void OnTriggerEnter2D(Collider2D other)
    {
        if (other.CompareTag("Ore"))
        {
            int oreCount = PlayerPrefs.GetInt(gameObject.tag + "OreCount", 0);
            oreCount++;
            PlayerPrefs.SetInt(gameObject.tag + "OreCount", oreCount);
            Destroy(other.gameObject, 0.01f);
        }
        if (other.CompareTag("Fuel"))
        {
            int fuelCount = PlayerPrefs.GetInt(gameObject.tag + "FuelCount", 0);
            fuelCount++;
            PlayerPrefs.SetInt(gameObject.tag + "FuelCount", fuelCount);
            Destroy(other.gameObject, 0.01f);
        }
        if (other.CompareTag("Rare"))
        {
            int rareCount = PlayerPrefs.GetInt(gameObject.tag + "RareCount", 0);
            rareCount++;
            PlayerPrefs.SetInt(gameObject.tag + "RareCount", rareCount);
            Destroy(other.gameObject, 0.01f);
        }

        GetComponent<PlayerController>().UpdateShipDisplay();
    }

    public void GetClosestResource ( )
    {
        GameObject nearestGO;
        foreach (GameObject go in targetResources )
        {
            //post
        }
        ///return 
    }
}
