﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class PlayerController : MonoBehaviour {

    public List<GameObject> shipPartsEnabled = new List<GameObject>();
    public List<GameObject> shipPartsDisabled = new List<GameObject>();

    public Spawner gameSpawner;

    public GameObject targetResource = null;
    float targetDistance = 10f;

    [Range(0, 100)]
    public float findOre = 0;
    [Range(0, 100)]
    public float findFuel = 0;
    [Range(0, 100)]
    public float findRare = 0;

    // Use this for initialization
    void Start() {

        foreach (GameObject shipPart in shipPartsDisabled)
        {
            shipPart.SetActive(true);
        }

        foreach (GameObject shipPart in shipPartsEnabled)
        {
            shipPart.SetActive(false);
        }
        GameObject spawnGO = GameObject.FindGameObjectWithTag("Spawner");
        gameSpawner = spawnGO.GetComponent<Spawner>();

        InvokeRepeating("CheckChances", 1f, 1f);
    }
    public Animator russiaLaunch;

    public Slider sliderOreCount;
    public Slider sliderFuelCount;
    public Slider sliderRareCount;
    public Slider sliderChances;
    // Update is called once per frame
    void Update()
    {
        bool triedOre = false;
        bool triedFuel = false;
        bool triedRare = false;
        bool foundSuccess = false;
        do
        {
            if (gameSpawner && targetResource == null)
            {
                if (findOre > findFuel && triedOre == false)
                {
                    if (findOre > findRare && triedOre == false)
                    {
                        // TODO FIND ORE!
                        foundSuccess = FindOre();
                        triedOre = true;
                    }
                }
                else if (findFuel > findRare && triedFuel == false)
                {
                    // TODO Find Fuel
                    foundSuccess = FindFuel();
                    triedFuel = true;
                }
                else if (triedRare == false)
                {
                    // TODO Find Rare
                    foundSuccess = FindRare();
                    triedRare = true;
                }
                else
                {
                    foundSuccess = false;
                }
                if (Input.anyKey)
                {
                    break;
                }
            }
        } while (foundSuccess);

    }
    // CheckChances is called once per second
    void CheckChances()
    {
        if (successpercent > 50)
        {
            ProcessRussia();
        }
    }



    bool FindOre()
    {
        float shortestDistance = targetDistance;
        GameObject target = null;
        foreach ( GameObject ore in gameSpawner.oreSpawned )
        {
            if (ore)
            {
                float thisDistance = Vector3.Distance(transform.position, ore.transform.position);
                if (thisDistance < shortestDistance)
                {
                    shortestDistance = thisDistance;
                    target = ore;
                }
            }
        }        
        targetResource = target;
        return (target == null);
    }
    bool FindFuel()
    {
        float shortestDistance = targetDistance;
        GameObject target = null;
        foreach (GameObject fuel in gameSpawner.fuelSpawned)
        {
            if (fuel)
            {
                float thisDistance = Vector3.Distance(transform.position, fuel.transform.position);
                if (thisDistance < shortestDistance)
                {
                    shortestDistance = thisDistance;
                    target = fuel;
                }
            }
        }
        targetResource = target;
        return (target == null);
    }
    bool FindRare()
    {
        float shortestDistance = targetDistance;
        GameObject target = null;
        foreach (GameObject rare in gameSpawner.rareSpawned)
        {
            if (rare)
            {
                float thisDistance = Vector3.Distance(transform.position, rare.transform.position);
                if (thisDistance < shortestDistance)
                {
                    shortestDistance = thisDistance;
                    target = rare;
                }
            }
        }
        targetResource = target;
        return (target == null);
    }
    void OnTriggerEnter2D(Collider2D other) 
	{
		if (other.CompareTag("Ore") ) 
		{
            int oreCount = PlayerPrefs.GetInt( gameObject.tag + "OreCount", 0);
            oreCount++;
            PlayerPrefs.SetInt(gameObject.tag + "OreCount", oreCount);

            if (gameSpawner)
            {
                gameSpawner.oreSpawned.Remove(other.gameObject);
            }
            targetResource = null;
			Destroy (other.gameObject, 0.01f);
		}
		if (other.CompareTag("Fuel")) 
		{
            int fuelCount = PlayerPrefs.GetInt(gameObject.tag + "FuelCount", 0);
            fuelCount++;
            PlayerPrefs.SetInt(gameObject.tag + "FuelCount", fuelCount);
            if (gameSpawner)
            {
                gameSpawner.fuelSpawned.Remove(other.gameObject);
            }
            targetResource = null;
            Destroy(other.gameObject, 0.01f);
        }
        if (other.CompareTag("Rare")) 
		{
            int rareCount = PlayerPrefs.GetInt(gameObject.tag + "RareCount", 0);
            rareCount++;
            PlayerPrefs.SetInt(gameObject.tag + "RareCount", rareCount);
            if (gameSpawner)
            {
                gameSpawner.rareSpawned.Remove(other.gameObject);
            }
            Destroy(other.gameObject, 0.01f);
            targetResource = null;
        }

		UpdateShipDisplay ();
	}
	public int maxOreCount = 5;
	public int maxFuelCount = 3;
	public int maxStuffCount = 5;

	public int minOreCount = 3;
	public int minFuelCount = 1;
	public int minStuffCount = 0;

	public int successpercent = 0;

    public int minSuccessChance = 80;

    public void UpdateShipDisplay()
	{

		successpercent = 100;
        if (sliderOreCount)
        {
            sliderOreCount.value = PlayerPrefs.GetInt(gameObject.tag + "OreCount", 0);

            successpercent = (int)(successpercent * (sliderOreCount.value / sliderOreCount.maxValue));
        }
        if (sliderFuelCount)
        {
            sliderFuelCount.value = PlayerPrefs.GetInt(gameObject.tag + "FuelCount", 0);
            successpercent = (int)(successpercent * (sliderFuelCount.value / sliderFuelCount.maxValue));
        }
        if (sliderRareCount)
        {
            sliderRareCount.value = PlayerPrefs.GetInt(gameObject.tag + "RareCount", 0);
            
            successpercent = (int)(successpercent * (sliderRareCount.value / sliderRareCount.maxValue));
        }

        if (sliderChances )
        {
            PlayerPrefs.SetInt( gameObject.tag + "successpercent", successpercent );
            
            sliderChances.value = successpercent;
        }

        for (int i = 0; i < shipPartsEnabled.Count; ++i)
        {
            if (successpercent > ((i + 1) * 15 ))
            {
                shipPartsEnabled[i].SetActive(true);
            }
            else
            {
                shipPartsEnabled[i].SetActive(false);
            }
        }

	}

    void OnSelected()
    {
        
    }

	public void EndGame()
    {
        int randomSuccess = Random.Range(0, 100);

        int successChance = PlayerPrefs.GetInt( gameObject.tag + "successpercent", 0 );
        bool successOccured = randomSuccess < successChance;
		bool successFailed = randomSuccess > successChance;
        if ( successOccured )
        {
			UnityEngine.SceneManagement.SceneManager.LoadScene(4);
        }
		if ( successFailed )
		{
			UnityEngine.SceneManagement.SceneManager.LoadScene(3);		
		}
    }

    public void ProcessRussia()
    {
        if (russiaLaunch != null)
        {

            int randomSuccess = Random.Range(50, 100);

            int successChance = PlayerPrefs.GetInt(gameObject.tag + "successpercent", 0);
            bool successOccured = randomSuccess < successChance;
            bool successFailed = randomSuccess > successChance;
            if (successChance > minSuccessChance)
            {
                if (successOccured)
                {
                    UnityEngine.SceneManagement.SceneManager.LoadScene(6);
                }
                if (successFailed)
                {
                    UnityEngine.SceneManagement.SceneManager.LoadScene(5);
                }
            }
            else
            {
                russiaLaunch.SetTrigger("Blink");
            }
        }
    }

} 
