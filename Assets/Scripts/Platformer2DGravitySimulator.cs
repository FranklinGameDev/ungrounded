﻿using UnityEngine;
using System.Collections;

public class Platformer2DGravitySimulator : MonoBehaviour {

    public float gravitySpeed = 10f;
    public Vector3 gravityDirection;

    public Transform gravityHost;

    public Rigidbody2D ourRB2d;

	// Use this for initialization
	void Start () {
        ourRB2d = GetComponent<Rigidbody2D>();
	}
	
	// Update is called once per frame
	void Update () {

        transform.up = (transform.position - gravityHost.position);
	    if ( ourRB2d )
        {
            ourRB2d.AddForce( Time.deltaTime * gravitySpeed * -1f * transform.up);
        }
	}
}
