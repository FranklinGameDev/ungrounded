using System;
using UnityEngine;
using UnityStandardAssets.CrossPlatformInput;

    [RequireComponent(typeof(Platformer2DLogic))]
    public class Platformer2DAIControl : MonoBehaviour
    {
        private Platformer2DLogic m_Character;

    // With smart AI, the ai somehow knows which resource to collect.
    public bool smartAI = true;

    PlayerController ourPlayerController;

    public GameObject gamePivot;


    // -1f Move Right, 0f No Movement, 1f Move Left
    private float movementDir = 0f;
        private bool m_Jump;

        [Range(0, 100)]
        public int jumpChance = 50;

        float jumpTimer = 0f;

        public float jumpTimeMin = 10.0f, jumpTimeMax = 1.0f;

        [Range(0, 100)]
        public int changeDirChance = 50;
        [Range(0, 100)]
        public int stopMovementChance = 50;
        [Range(0.1f, 1.0f)]
        public float movementStoppedModifier = 0.5f;
        [Range(0, 100)]
        public int changeDirLeftChance = 50;

        float changeDirTimer = 0f;

        public float changeDirTimeMin = 1.0f, changeDirTimeMax = 5.0f;



        private void Awake()
        {
            m_Character = GetComponent<Platformer2DLogic>();
            jumpTimer = UnityEngine.Random.Range(jumpTimeMin, jumpTimeMax);

            ourPlayerController = GetComponent<PlayerController>();
        }


        private void Update()
        {
            // If we're jumping don't try to jump
            if (!m_Jump)
            {
                // Has our timer run out and should we then test to see if AI sould jump?
                if (jumpTimer < 0)
                {
                    //Get a random to see if we should jump
                    float successChance = UnityEngine.Random.Range(1, 100);

                    // If our random is larger than our defined jump chance, then JUMP!
                    if (successChance < jumpChance)
                    {
                        m_Jump = true;
                    }

                    // if we jump or not, we still need to reset our timer to see if we want to jump again next time.
                    jumpTimer = UnityEngine.Random.Range(jumpTimeMin, jumpTimeMax);
                }
                else
                {
                    // if we didn't run out of time in our jump timer, subtract how much "time" has elapsed in the last CPU cycle and move along until the next time this code is ran
                    jumpTimer -= Time.deltaTime;
                }
            }

            // Did our change direction timer run out of time?
            if (changeDirTimer < 0)
            {
                // should we change direction?
                float successChance = UnityEngine.Random.Range(1, 100);

                if (successChance < changeDirChance)
                {
                    // Should we stop movement for a moment
                    successChance = UnityEngine.Random.Range(1, 100);
                if (successChance < stopMovementChance)
                {
                    movementDir = 0f;
                }
                else
                {
                    // Should we go left?
                    successChance = UnityEngine.Random.Range(1, 100);
                    if (smartAI  && gamePivot && ourPlayerController.targetResource )
                    {
                        Vector3 towardsResource = gamePivot.transform.position - ourPlayerController.targetResource.transform.position;
                        Vector3 towardsSelf = gamePivot.transform.position - gameObject.transform.position;

                        float angleBetween = Vector3.Angle(towardsResource, towardsSelf);
                        Debug.Log("AngleBetween : " + angleBetween);

                        Vector3 cross = Vector3.Cross(towardsResource, Vector3.up);

                        if ( cross.z > 0 )
                        {
                            // Go Right
                            movementDir = 1f;
                        }
                        else
                        {
                            // Go Left
                            movementDir = -1f;
                        }
                    }
                    else
                    {
						Debug.Log ("THis code ran");
                        if (successChance < changeDirLeftChance)
                        {
                            movementDir = 1f;
                        }
                        else
                        {
                            // No Luck, Go right
                            movementDir = -1f;
                        }
                    }
                }
                }
            // If we have stopped, wait LESS time than usual before checking to change directions again
            float minRange = movementDir == 0 ? changeDirTimeMin * 0.25f : changeDirTimeMin;
            float maxRange = movementDir == 0 ? changeDirTimeMax * 0.25f : changeDirTimeMax;
            changeDirTimer = UnityEngine.Random.Range( minRange, maxRange );
            }
            else
            {
                changeDirTimer -= Time.deltaTime;
            }
        }


        private void FixedUpdate()
        {
            // Read the inputs.
            bool crouch = false;//Input.GetKey(KeyCode.LeftControl);
            // Pass all parameters to the character control script.
            m_Character.Move(movementDir, crouch, m_Jump);
            m_Jump = false;
        }
    }