﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class Spawner : MonoBehaviour
{

    public int spawnCountOreMin = 5;
    public int spawnCountOreMax = 13;

    public int spawnCountFuelMin = 3;
    public int spawnCountFuelMax = 7;

    public int spawnCountRareMin = 0;
    public int spawnCountRareMax = 3;

    public GameObject oreGO = null;
    public GameObject fuelGO = null;
    public GameObject rareGO = null;

    public List<GameObject> oreSpawned = new List<GameObject>();
    public List<GameObject> fuelSpawned = new List<GameObject>();
    public List<GameObject> rareSpawned = new List<GameObject>();

    // Use this for initialization
    void Start()
    {
        if (oreGO == null ||
            fuelGO == null ||
            rareGO == null)
        {
            Debug.LogError("Spawn Object not setup for spawning.");
            return;
        }

        InvokeRepeating("Spawn", 5f, 25f );
    }

    void Spawn ()
    {
        int spawncount = Random.Range(spawnCountOreMin, spawnCountOreMax);

        for (int i = 0; i < spawncount; ++i)
        {
            GameObject newGO = Instantiate(oreGO,transform.position, transform.rotation);
			//Debug.Log ("Postition ="+ newGO.transform.position +"rotation = "+ newGO.transform.rotation);
            newGO.SetActive(true);
            oreSpawned.Add(newGO);
            Rigidbody2D newRb = newGO.GetComponent<Rigidbody2D>();
            if (newRb)
            {
                newRb.WakeUp();
            }
        }

        spawncount = Random.Range(spawnCountFuelMin, spawnCountFuelMax);
        for (int i = 0; i < spawncount; ++i)
        {
            GameObject newGO = Instantiate(fuelGO, transform.position, transform.rotation);
            newGO.SetActive(true);
            fuelSpawned.Add(newGO);
            Rigidbody2D newRb = newGO.GetComponent<Rigidbody2D>();
            if (newRb)
            {
                newRb.WakeUp();
            }
        }

        spawncount = Random.Range(spawnCountRareMin, spawnCountRareMax);
        for (int i = 0; i < spawncount; ++i)
        {
            GameObject newGO = Instantiate(rareGO, transform.position, transform.rotation);
            newGO.SetActive(true);
            rareSpawned.Add(newGO);
            Rigidbody2D newRb = newGO.GetComponent<Rigidbody2D>();
            if (newRb)
            {
                newRb.WakeUp();
            }
        }

    }

}