﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PivotAround : MonoBehaviour {

    public Transform pivot;

    public Vector3 pivotAxis = Vector3.forward;

    public float pivotSpeed = 10f;

	// Update is called once per frame
	void Update ()
    {
        transform.RotateAround(pivot.position, pivotAxis, Time.deltaTime * pivotSpeed );
	}
}
