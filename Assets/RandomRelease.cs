﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityStandardAssets.Utility;

[RequireComponent(typeof( FollowTarget))]
public class RandomRelease : MonoBehaviour {

    float randomRelaseTimer = 0f;
    public float randomRelaseMin = 10.0f;
    public float randomRelaseMax = 100.0f;

    FollowTarget ourFollowTarget;

    // Use this for initialization
    void Start ()
    {
        Invoke( "Release", Random.Range(randomRelaseMin, randomRelaseMax) );

        ourFollowTarget = GetComponent<FollowTarget>();
	}
	
	// Update is called once per frame
	void Release () {
        ourFollowTarget.Release();
	}
}
